## scala-dev-container ##

This container allows the user to use it as a development environment for scala projects. 

How to use:

```
docker container create --name scaladev -v "$(PWD):/home/sbtuser" sanketnaik/scala-dev-container
docker container start scaladev
```

#### Using it with compose ####

```
version: '3'
volumes:
  scalavol:
    driver: local
    driver_opts:
      o: bind
      type: none
      device: $HOME/scaladev

services:
  scaladev:
    image: sanketnaik/scala-dev-container
    volumes:
      - scalavol: /home/sbtuser
```
The above is a sample compose file. 

```
Please ensure the directory $HOME/scaladev already exists before you do docker-compose up
```

#### Using for Developoment with vscode ####

Once the stack/container is up, then you can follow the instructions [here](https://code.visualstudio.com/docs/remote/containers#_attaching-to-running-containers) to attach Visual Studio Code and use this container as a development environment.
