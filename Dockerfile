ARG SCALA_SBT_VERSION=11.0.6_1.3.10_2.13.1
FROM hseeberger/scala-sbt:${SCALA_SBT_VERSION}

LABEL maintainer="Sanket Naik <sanketn@gmail.com>"
ENV TINI_VERSION v0.19.0
ADD https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini /tini
RUN chmod +x /tini
ENTRYPOINT ["/tini", "--", "tail", "-f", "/dev/null"]

WORKDIR "/home/sbtuser"
USER "sbtuser"